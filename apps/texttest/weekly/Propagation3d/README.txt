NOTICE:
-------

The following files included in this folder:

	* heart_chaste2_renum_i_triangles.node   - Tetgen format node file
	* heart_chaste2_renum_i_triangles.ele    - Tetgen format element file
	* heart_chaste2_renum_i_triangles.face   - Tetgen format face file
	* heart_chaste2_renum_i_triangles.epi    - Indices of nodes on the epicardium
	* heart_chaste2_renum_i_triangles.lv     - Indices of nodes on the left ventricular surface
	* heart_chaste2_renum_i_triangles.rv     - Indices of nodes on the right ventricular surface
	* heart_chaste2_renum_i_triangles.axi    - Normalised fibre directions, one per element (first line gives number of elements).  Taken from original segmentation

All nodes are indexed from zero.

This mesh is a downsampled version of the Oxford rabbit cardiac model: Bishop et al. Martin J. Bishop, Gernot Plank, Rebecca A. B. Burton, Jürgen E. Schneider, David J. Gavaghan, Vicente Grau, and Peter Kohl "Development of an anatomically detailed MRI-derived rabbit ventricular model and assessment of its impact on simulations of electrophysiological function" (Am J Physiol Heart Circ Physiol, 2010)
The full-resolution mesh has been released as Open Source data and is available to download from www.comlab.ox.ac.uk/chaste

We would like to ask any user:
	- not to re-distribute the mesh, but point any potential user to www.comlab.ox.ac.uk/chaste for our own tracking purposes.
	- acknowledge source if used for presentation / publication. For details contact chaste-users@maillist.ox.ac.uk
