/*

Copyright (c) 2005-2012, University of Oxford.
All rights reserved.

University of Oxford means the Chancellor, Masters and Scholars of the
University of Oxford, having an administrative office at Wellington
Square, Oxford OX1 2JD, UK.

This file is part of Chaste.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 * Neither the name of the University of Oxford nor the names of its
   contributors may be used to endorse or promote products derived from this
   software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef TESTCOMMANDLINEARGUMENTS_HPP_
#define TESTCOMMANDLINEARGUMENTS_HPP_

#include <cxxtest/TestSuite.h>
#include <iostream>
#include <cassert>
#include "CommandLineArguments.hpp"

/* HOW_TO_TAG General
 * Read and use parameters from the command line
 *
 * If your want to use parameters that are supplied in the command line, then
 *  (i) add lines such as "double x = CommandLineArguments::Instance()->GetDoubleCorrespondingToOption("-myparam");" below
 *  (ii) use scons to compile but not run the test (see ChasteGuides/RunningBinariesFromCommandLine)
 *  (iii) run the compiled executable from the command line (see ChasteGuides/RunningBinariesFromCommandLine), with your parameter
 *
 *  Eg:
 *  scons co=1 ts=projects/you/TestBlah.hpp
 *  ./projects/you/build/debug/TestBlahRunner -myparam 10.4
 *
 * Note: error messages such as
 *   WARNING! There are options you set that were not used!
 *   WARNING! could be spelling mistake, etc!
 * are due to Petsc thinking the parameter must have been for it.
 *
 */

class TestCommandLineArguments : public CxxTest::TestSuite
{
public:

    void TestCommandLineArgumentsSingleton() throw(Exception)
    {
        // Test that argc and argv are populated
        int argc = *(CommandLineArguments::Instance()->p_argc);
        TS_ASSERT_LESS_THAN(0, argc); // argc should always be 1 or greater

        // argv[0] will be equal to global/build/debug/TestCommandLineArgumentsRunner
        // or global/build/optimised/TestCommandLineArgumentsRunner, etc
        char** argv = *(CommandLineArguments::Instance()->p_argv);
        assert(argv != NULL);
        std::string arg_as_string(argv[0]);
        std::string final_part_of_string = arg_as_string.substr(arg_as_string.length()-30,arg_as_string.length());
        TS_ASSERT_EQUALS("TestCommandLineArgumentsRunner",final_part_of_string);

        // Now test OptionExists() and GetValueCorrespondingToOption()
        //
        // The following tests would require the following arguments to be passed
        // in:
        // ./global/build/debug/TestCommandLineArgumentsRunner -myoption -myintval 24 -mydoubleval 3.14 -3.14 -m2intval -42 -mystrings Baboons Monkeys Gibbons -mystring more_baboons
        //
        // To test the methods we overwrite the arg_c and arg_v contained in the
        // singleton with the arguments that were needed.
        int new_argc = 15;
        char new_argv0[] = "..";
        char new_argv1[] = "-myoption";
        char new_argv2[] = "-myintval";
        char new_argv3[] = "24";
        char new_argv4[] = "-mydoubleval";
        char new_argv5[] = "3.14";
        char new_argv6[] = "-3.14";
        char new_argv7[] = "-m2intval";
        char new_argv8[] = "-42";
        char new_argv9[] = "-mystrings";
        char new_argv10[] = "Baboons";
        char new_argv11[] = "Monkeys";
        char new_argv12[] = "Gibbons";
        char new_argv13[] = "-mystring";
        char new_argv14[] = "more_baboons";

        char** new_argv = new char*[15];
        new_argv[0] = new_argv0;
        new_argv[1] = new_argv1;
        new_argv[2] = new_argv2;
        new_argv[3] = new_argv3;
        new_argv[4] = new_argv4;
        new_argv[5] = new_argv5;
        new_argv[6] = new_argv6;
        new_argv[7] = new_argv7;
        new_argv[8] = new_argv8;
        new_argv[9] = new_argv9;
        new_argv[10] = new_argv10;
        new_argv[11] = new_argv11;
        new_argv[12] = new_argv12;
        new_argv[13] = new_argv13;
        new_argv[14] = new_argv14;

        // Save the real args to be restored at the end
        int* p_real_argc = CommandLineArguments::Instance()->p_argc;
        char*** p_real_argv = CommandLineArguments::Instance()->p_argv;

        // Overwrite the args
        CommandLineArguments::Instance()->p_argc = &new_argc;
        CommandLineArguments::Instance()->p_argv = &new_argv;

        // Test OptionExists()
        TS_ASSERT(CommandLineArguments::Instance()->OptionExists("-myoption"));

        TS_ASSERT( ! CommandLineArguments::Instance()->OptionExists("-asddsgijdfgokgfgurgher"));

        TS_ASSERT_THROWS_THIS(CommandLineArguments::Instance()->OptionExists("-42"),
                "A command line option must begin with '-' followed by a non-numeric character.");

        TS_ASSERT_THROWS_THIS(CommandLineArguments::Instance()->GetStringsCorrespondingToOption("-myoption"),
                "No value(s) given after command line option '-myoption'");

        TS_ASSERT_THROWS_THIS(CommandLineArguments::Instance()->GetStringsCorrespondingToOption("-mynonsense"),
                "Command line option '-mynonsense' does not exist");


        // Test GetValueCorrespondingToOption()
        char* val = CommandLineArguments::Instance()->GetValueCorrespondingToOption("-myintval");
        unsigned i = atol(val);
        TS_ASSERT_EQUALS(i, 24u);


        val = CommandLineArguments::Instance()->GetValueCorrespondingToOption("-m2intval");
        int j = atol(val);
        TS_ASSERT_EQUALS(j, -42);

        j = CommandLineArguments::Instance()->GetIntCorrespondingToOption("-m2intval");
        TS_ASSERT_EQUALS(j, -42);

        TS_ASSERT_THROWS_THIS(i = CommandLineArguments::Instance()->GetUnsignedCorrespondingToOption("-m2intval"),
                              "Option is a negative number and cannot be converted to unsigned.");

        i = CommandLineArguments::Instance()->GetUnsignedCorrespondingToOption("-myintval");
        TS_ASSERT_EQUALS(i, 24u);

        val = CommandLineArguments::Instance()->GetValueCorrespondingToOption("-mydoubleval");
        double x = atof(val);
        TS_ASSERT_EQUALS(x, 3.14);

        x = CommandLineArguments::Instance()->GetDoubleCorrespondingToOption("-mydoubleval");
        TS_ASSERT_EQUALS(x, 3.14);

        // Test exceptions in GetValueCorrespondingToOption()
        TS_ASSERT_THROWS_CONTAINS(CommandLineArguments::Instance()->GetValueCorrespondingToOption("-rwesdb"), "does not exist");


        val = CommandLineArguments::Instance()->GetValueCorrespondingToOption("-mystring");
        std::string argument(val);
        TS_ASSERT_EQUALS(argument, "more_baboons");

        std::string string_argument = CommandLineArguments::Instance()->GetStringCorrespondingToOption("-mystring");
        TS_ASSERT_EQUALS(string_argument, "more_baboons");

        std::vector<std::string> string_arguments = CommandLineArguments::Instance()->GetStringsCorrespondingToOption("-mystrings");
        TS_ASSERT_EQUALS(string_arguments.size(), 3u);
        TS_ASSERT_EQUALS(string_arguments[0], "Baboons");
        TS_ASSERT_EQUALS(string_arguments[1], "Monkeys");
        TS_ASSERT_EQUALS(string_arguments[2], "Gibbons");

        std::vector<double> double_arguments = CommandLineArguments::Instance()->GetDoublesCorrespondingToOption("-mydoubleval");
        TS_ASSERT_EQUALS(double_arguments.size(), 2u);
        TS_ASSERT_DELTA(double_arguments[0], 3.14, 1e-6);
        TS_ASSERT_DELTA(double_arguments[1], -3.14, 1e-6);

        std::vector<unsigned> unsigned_args = CommandLineArguments::Instance()->GetUnsignedsCorrespondingToOption("-myintval");
        TS_ASSERT_EQUALS(unsigned_args.size(), 1u);
        TS_ASSERT_EQUALS(unsigned_args[0],24u);

        std::vector<int> int_args = CommandLineArguments::Instance()->GetIntsCorrespondingToOption("-m2intval");
        TS_ASSERT_EQUALS(int_args.size(), 1u);
        TS_ASSERT_EQUALS(int_args[0],-42);

        // Fool the arguments into thinking that the options end at 5 entries to check an exception:
        new_argc = 5;
        TS_ASSERT_THROWS_THIS(CommandLineArguments::Instance()->GetValueCorrespondingToOption("-mydoubleval"),
                "No value(s) given after command line option '-mydoubleval'");

        delete new_argv;

        // Restore the real args
        CommandLineArguments::Instance()->p_argc = p_real_argc;
        CommandLineArguments::Instance()->p_argv = p_real_argv;
    }
};

#endif /*TESTCOMMANDLINEARGUMENTS_HPP_*/
