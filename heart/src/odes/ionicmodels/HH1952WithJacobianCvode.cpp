#ifdef CHASTE_CVODE
//! @file
//! 
//! This source file was generated from CellML.
//! 
//! Model: hodgkin_huxley_squid_axon_model_1952_modified
//! 
//! Processed by pycml - CellML Tools in Python
//!     (translators: 16411, pycml: 16306, optimize: 14997)
//! on Sun Aug  5 18:04:13 2012
//! 
//! <autogenerated>

#define COVERAGE_IGNORE

#include "HH1952WithJacobianCvode.hpp"
#include <cmath>
#include <cassert>
#include <memory>
#include "Exception.hpp"
#include "OdeSystemInformation.hpp"
#include "RegularStimulus.hpp"
#include "HeartConfig.hpp"
#include "IsNan.hpp"

    double HH1952WithJacobianCvode::Get_chaste_interface__i_ionic()
    {
        return var_chaste_interface__i_ionic;
    }
    
    double HH1952WithJacobianCvode::Get_membrane__Cm()
    {
        return var_membrane__Cm;
    }
    
    double HH1952WithJacobianCvode::Get_chaste_interface__membrane__i_Stim()
    {
        return var_chaste_interface__membrane__i_Stim;
    }
    
    double HH1952WithJacobianCvode::Get_chaste_interface__membrane__V()
    {
        return var_chaste_interface__membrane__V;
    }
    
    HH1952WithJacobianCvode::HH1952WithJacobianCvode(boost::shared_ptr<AbstractIvpOdeSolver> pOdeSolver /* unused; should be empty */, boost::shared_ptr<AbstractStimulusFunction> pIntracellularStimulus)
        : AbstractCvodeCell(
                pOdeSolver,
                4,
                0,
                pIntracellularStimulus)
    {
        // Time units: millisecond
        // 
        this->mpSystemInfo = OdeSystemInformation<HH1952WithJacobianCvode>::Instance();
        Init();
        this->mUseAnalyticJacobian = true;
    }
    
    HH1952WithJacobianCvode::~HH1952WithJacobianCvode()
    {
    }
    
    void HH1952WithJacobianCvode::VerifyStateVariables()
    {
        
    }
    
    double HH1952WithJacobianCvode::GetIIonic(const std::vector<double>* pStateVariables)
    {
        // For state variable interpolation (SVI) we read in interpolated state variables,
        // otherwise for ionic current interpolation (ICI) we use the state variables of this model (node).
        N_Vector rY;
        bool made_new_cvode_vector = false;
        if (!pStateVariables)
        {
            rY = rGetStateVariables();
        }
        else
        {
            made_new_cvode_vector = true;
            rY = MakeNVector(*pStateVariables);
        }
        
        double var_chaste_interface__membrane__V = (mSetVoltageDerivativeToZero ? this->mFixedVoltage : NV_Ith_S(rY, 0));
        // Units: millivolt; Initial value: -75
        double var_chaste_interface__sodium_channel_m_gate__m = NV_Ith_S(rY, 1);
        // Units: dimensionless; Initial value: 0.05
        double var_chaste_interface__sodium_channel_h_gate__h = NV_Ith_S(rY, 2);
        // Units: dimensionless; Initial value: 0.6
        double var_chaste_interface__potassium_channel_n_gate__n = NV_Ith_S(rY, 3);
        // Units: dimensionless; Initial value: 0.325
        
        const double var_sodium_channel__i_Na = 120.0 * pow(var_chaste_interface__sodium_channel_m_gate__m, 3.0) * var_chaste_interface__sodium_channel_h_gate__h * (var_chaste_interface__membrane__V - 40.0); // microA_per_cm2
        const double var_potassium_channel__i_K = 36.0 * pow(var_chaste_interface__potassium_channel_n_gate__n, 4.0) * (var_chaste_interface__membrane__V -  -87.0); // microA_per_cm2
        const double var_leakage_current__i_L = 0.3 * (var_chaste_interface__membrane__V -  -64.387); // microA_per_cm2
        var_chaste_interface__i_ionic = var_sodium_channel__i_Na + var_potassium_channel__i_K + var_leakage_current__i_L; // uA_per_cm2
        
        const double i_ionic = var_chaste_interface__i_ionic;
        if (made_new_cvode_vector)
        {
            DeleteVector(rY);
        }
        EXCEPT_IF_NOT(!std::isnan(i_ionic));
        return i_ionic;
    }
    
    void HH1952WithJacobianCvode::EvaluateYDerivatives(double var_chaste_interface__environment__time, const N_Vector rY, N_Vector rDY)
    {
        // Inputs:
        // Time units: millisecond
        double var_chaste_interface__membrane__V = (mSetVoltageDerivativeToZero ? this->mFixedVoltage : NV_Ith_S(rY, 0));
        // Units: millivolt; Initial value: -75
        double var_chaste_interface__sodium_channel_m_gate__m = NV_Ith_S(rY, 1);
        // Units: dimensionless; Initial value: 0.05
        double var_chaste_interface__sodium_channel_h_gate__h = NV_Ith_S(rY, 2);
        // Units: dimensionless; Initial value: 0.6
        double var_chaste_interface__potassium_channel_n_gate__n = NV_Ith_S(rY, 3);
        // Units: dimensionless; Initial value: 0.325
        
        // Mathematics
        double d_dt_chaste_interface__membrane__V;
        const double d_dt_chaste_interface__sodium_channel_m_gate__m = ((( -0.1 * (var_chaste_interface__membrane__V + 50.0)) / (exp((-(var_chaste_interface__membrane__V + 50.0)) * 0.1) - 1.0)) * (1.0 - var_chaste_interface__sodium_channel_m_gate__m))
                                                                       - ((4.0 * exp((-(var_chaste_interface__membrane__V + 75.0)) * 0.0555555555556)) * var_chaste_interface__sodium_channel_m_gate__m); // per_millisecond
        const double d_dt_chaste_interface__sodium_channel_h_gate__h = ((0.07 * exp((-(var_chaste_interface__membrane__V + 75.0)) * 0.05)) * (1.0 - var_chaste_interface__sodium_channel_h_gate__h))
                                                                       - ((1.0 / (exp((-(var_chaste_interface__membrane__V + 45.0)) * 0.1) + 1.0)) * var_chaste_interface__sodium_channel_h_gate__h); // per_millisecond
        const double d_dt_chaste_interface__potassium_channel_n_gate__n = ((( -0.01 * (var_chaste_interface__membrane__V + 65.0)) / (exp((-(var_chaste_interface__membrane__V + 65.0)) * 0.1) - 1.0)) * (1.0 - var_chaste_interface__potassium_channel_n_gate__n))
                                                                       - ((0.125 * exp((var_chaste_interface__membrane__V + 75.0) * 0.0125)) * var_chaste_interface__potassium_channel_n_gate__n); // per_millisecond
        
        if (mSetVoltageDerivativeToZero)
        {
            d_dt_chaste_interface__membrane__V = 0.0;
        }
        else
        {
            var_membrane__Cm = 1.0; // microF_per_cm2
            const double var_sodium_channel__i_Na = 120.0 * pow(var_chaste_interface__sodium_channel_m_gate__m, 3.0) * var_chaste_interface__sodium_channel_h_gate__h * (var_chaste_interface__membrane__V - 40.0); // microA_per_cm2
            const double var_potassium_channel__i_K = 36.0 * pow(var_chaste_interface__potassium_channel_n_gate__n, 4.0) * (var_chaste_interface__membrane__V -  -87.0); // microA_per_cm2
            const double var_leakage_current__i_L = 0.3 * (var_chaste_interface__membrane__V -  -64.387); // microA_per_cm2
            var_chaste_interface__membrane__i_Stim = GetIntracellularAreaStimulus(var_chaste_interface__environment__time);
            d_dt_chaste_interface__membrane__V = (-(var_chaste_interface__membrane__i_Stim + var_sodium_channel__i_Na + var_potassium_channel__i_K + var_leakage_current__i_L)) / var_membrane__Cm; // 'millivolt per millisecond'
        }
        
        NV_Ith_S(rDY, 0) = d_dt_chaste_interface__membrane__V;
        NV_Ith_S(rDY, 1) = d_dt_chaste_interface__sodium_channel_m_gate__m;
        NV_Ith_S(rDY, 2) = d_dt_chaste_interface__sodium_channel_h_gate__h;
        NV_Ith_S(rDY, 3) = d_dt_chaste_interface__potassium_channel_n_gate__n;
    }
    
    void HH1952WithJacobianCvode::EvaluateAnalyticJacobian(long int N, realtype time, N_Vector rY, N_Vector rDY,
#if CHASTE_SUNDIALS_VERSION >= 20400
                                          DlsMat Jac,
#else
                                          DenseMat Jac,
#endif
            N_Vector tmp1, N_Vector tmp2, N_Vector tmp3)
    {
        double var_chaste_interface__membrane__V = (mSetVoltageDerivativeToZero ? this->mFixedVoltage : NV_Ith_S(rY, 0));
        double var_chaste_interface__sodium_channel_m_gate__m = NV_Ith_S(rY, 1);
        // Units: dimensionless; Initial value: 0.05
        double var_chaste_interface__sodium_channel_h_gate__h = NV_Ith_S(rY, 2);
        // Units: dimensionless; Initial value: 0.6
        double var_chaste_interface__potassium_channel_n_gate__n = NV_Ith_S(rY, 3);
        // Units: dimensionless; Initial value: 0.325

        // d (Vdot)
        if (mSetVoltageDerivativeToZero)
        {
            DENSE_ELEM(Jac,0,0) = 0.0; // dV'/dV
            DENSE_ELEM(Jac,0,1) = 0.0; // dV'/dm
            DENSE_ELEM(Jac,0,2) = 0.0; // dV'/dh
            DENSE_ELEM(Jac,0,3) = 0.0; // dV'/dn
        }
        else
        {
            DENSE_ELEM(Jac,0,0) = (-1.0/var_membrane__Cm)*(120.0 * pow(var_chaste_interface__sodium_channel_m_gate__m, 3.0) * var_chaste_interface__sodium_channel_h_gate__h
                                                                 + 36.0 * pow(var_chaste_interface__potassium_channel_n_gate__n, 4.0)
                                                                 + 0.3); // dV'/dV
            DENSE_ELEM(Jac,0,1) = (-1.0/var_membrane__Cm)*(3.0*120.0 * pow(var_chaste_interface__sodium_channel_m_gate__m, 2.0) * var_chaste_interface__sodium_channel_h_gate__h * (var_chaste_interface__membrane__V - 40.0)); // dV'/dm
            DENSE_ELEM(Jac,0,2) = (-1.0/var_membrane__Cm)*(120.0 * pow(var_chaste_interface__sodium_channel_m_gate__m, 3.0) * (var_chaste_interface__membrane__V - 40.0)); // dV'/dh
            DENSE_ELEM(Jac,0,3) = (-1.0/var_membrane__Cm)*(4.0*36.0 * pow(var_chaste_interface__potassium_channel_n_gate__n, 3.0) * (var_chaste_interface__membrane__V -  -87.0)); // dV'/dn
        }

        // d(m dot)
        DENSE_ELEM(Jac,1,0) = ((((-0.1)*(exp((-(var_chaste_interface__membrane__V + 50.0)) * 0.1) - 1.0)
                              - (-0.1*exp((-(var_chaste_interface__membrane__V + 50.0)) * 0.1))*(-0.1 * (var_chaste_interface__membrane__V + 50.0)) )
                               / pow(exp((-(var_chaste_interface__membrane__V + 50.0)) * 0.1) - 1.0,2.0)) * (1.0 - var_chaste_interface__sodium_channel_m_gate__m))
                              - ((4.0 * -0.0555555555556 * exp((-(var_chaste_interface__membrane__V + 75.0)) * 0.0555555555556)) * var_chaste_interface__sodium_channel_m_gate__m); // dm'/dV
        DENSE_ELEM(Jac,1,1) = -(( -0.1 * (var_chaste_interface__membrane__V + 50.0)) / (exp((-(var_chaste_interface__membrane__V + 50.0)) * 0.1) - 1.0))
                -(4.0 * exp((-(var_chaste_interface__membrane__V + 75.0)) * 0.0555555555556)); // dm'/dm
        DENSE_ELEM(Jac,1,2) = 0.0; // dm'/dh
        DENSE_ELEM(Jac,1,3) = 0.0; // dm'/dn

        // d(h dot)
        DENSE_ELEM(Jac,2,0) = ((0.07* -0.05 * exp((-(var_chaste_interface__membrane__V + 75.0)) * 0.05)) * (1.0 - var_chaste_interface__sodium_channel_h_gate__h))
                              - ((1.0 * 0.1 / (exp((-(var_chaste_interface__membrane__V + 45.0)) * 0.1) + 1.0)) * var_chaste_interface__sodium_channel_h_gate__h); // dh'/dV
        DENSE_ELEM(Jac,2,1) = 0.0; // dh'/dm
        DENSE_ELEM(Jac,2,2) = -(0.07 * exp((-(var_chaste_interface__membrane__V + 75.0)) * 0.05))
                - (1.0 / (exp((-(var_chaste_interface__membrane__V + 45.0)) * 0.1) + 1.0)) ; // dh'/dh
        DENSE_ELEM(Jac,2,3) = 0.0; // dh'/dn

        // d(n dot)
        DENSE_ELEM(Jac,3,0) = ((( -0.01) * (exp((-(var_chaste_interface__membrane__V + 65.0)) * 0.1) - 1.0)) - (-0.01 * (var_chaste_interface__membrane__V + 65.0))*( 0.1*exp((-(var_chaste_interface__membrane__V + 65.0)) * 0.1))
                         / (pow(exp((-(var_chaste_interface__membrane__V + 65.0)) * 0.1) - 1.0,2.0)) * (1.0 - var_chaste_interface__potassium_channel_n_gate__n))
                        - ((0.125 * 0.0125 * exp((var_chaste_interface__membrane__V + 75.0) * 0.0125)) * var_chaste_interface__potassium_channel_n_gate__n); // dn'/dV
        DENSE_ELEM(Jac,3,1) = 0.0; // dn'/dm
        DENSE_ELEM(Jac,3,2) = 0.0; // dn'/dh
        DENSE_ELEM(Jac,3,3) =  -( -0.01 * (var_chaste_interface__membrane__V + 65.0)) / (exp((-(var_chaste_interface__membrane__V + 65.0)) * 0.1) - 1.0)
                - (0.125 * exp((var_chaste_interface__membrane__V + 75.0) * 0.0125)) ; // dn'/dn
    }

    N_Vector HH1952WithJacobianCvode::ComputeDerivedQuantities(double var_chaste_interface__environment__time, const N_Vector & rY)
    {
        // Inputs:
        // Time units: millisecond
        
        
        // Mathematics
        var_chaste_interface__membrane__i_Stim = GetIntracellularAreaStimulus(var_chaste_interface__environment__time);
        
        N_Vector dqs = N_VNew_Serial(1);
        NV_Ith_S(dqs, 0) = var_chaste_interface__membrane__i_Stim;
        return dqs;
    }
    
template<>
void OdeSystemInformation<HH1952WithJacobianCvode>::Initialise(void)
{
    this->mSystemName = "hodgkin_huxley_squid_axon_model_1952_modified";
    this->mFreeVariableName = "environment__time";
    this->mFreeVariableUnits = "millisecond";
    
    this->mVariableNames.push_back("membrane_voltage");
    this->mVariableUnits.push_back("millivolt");
    this->mInitialConditions.push_back(-75);

    this->mVariableNames.push_back("sodium_channel_m_gate__m");
    this->mVariableUnits.push_back("dimensionless");
    this->mInitialConditions.push_back(0.05);

    this->mVariableNames.push_back("sodium_channel_h_gate__h");
    this->mVariableUnits.push_back("dimensionless");
    this->mInitialConditions.push_back(0.6);

    this->mVariableNames.push_back("potassium_channel_n_gate__n");
    this->mVariableUnits.push_back("dimensionless");
    this->mInitialConditions.push_back(0.325);

    this->mDerivedQuantityNames.push_back("membrane_stimulus_current");
    this->mDerivedQuantityUnits.push_back("uA_per_cm2");
    
    this->mInitialised = true;
}


// Serialization for Boost >= 1.36
#include "SerializationExportWrapperForCpp.hpp"
CHASTE_CLASS_EXPORT(HH1952WithJacobianCvode)

#undef COVERAGE_IGNORE

#endif // CHASTE_CVODE


